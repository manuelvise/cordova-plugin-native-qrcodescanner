package it.crispybacon.qrcodescanner;
import android.content.Intent;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class NativeQrcode extends CordovaPlugin {

    public final int QRCODE_OP = 11;
    private CallbackContext callback = null;
    public static final String CAMERA = Manifest.permission.CAMERA;
    public static final int CAMERA_REQ_CODE = 0;

    /**
     * Executes the request and returns a boolean.
     *
     * @param action
     *            The action to execute.
     * @param args
     *            JSONArry of arguments for the plugin.
     * @param callbackContext
     *            The callback context used when calling back into JavaScript.
     * @return boolean.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("open")) {
            try {
                callback = callbackContext;

                if(cordova.hasPermission(READ)){
                    openQrcodeActivity();
                }else{
                    cordova.requestPermission(this, CAMERA_REQ_CODE, CAMERA);
                }

                
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
        }
        return true;
    }

    private void openQrcodeActivity() throws IOException {

        

        cordova.setActivityResultCallback (this);

        Intent intent = new Intent(this.cordova.getActivity().getApplicationContext(), QrcodeScanner.class);
        cordova.startActivityForResult (this, intent, QRCODE_OP);
        //this.cordova.getActivity().overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        if( requestCode == QRCODE_OP )
        {
            if( resultCode == Activity.RESULT_OK && data.hasExtra("return_val") )
            {
                PluginResult result = new PluginResult(PluginResult.Status.OK, data.getStringExtra("return_val"));
                result.setKeepCallback(true);
                callback.sendPluginResult(result);
            }
            else
            {
                PluginResult result = new PluginResult(PluginResult.Status.ERROR, "NO_RESULT_ERROR" );
                result.setKeepCallback(true);
                callback.sendPluginResult(result);
            }
        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                         int[] grantResults) throws JSONException
    {
        for(int r:grantResults)
        {
            if(r == PackageManager.PERMISSION_DENIED)
            {
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "PERMISSION_DENIED_ERROR"));
                return;
            }
        }
        switch(requestCode)
        {
            case CAMERA_REQ_CODE:
                openQrcodeActivity();
                break;
        }
    }
}
