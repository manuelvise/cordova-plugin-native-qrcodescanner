

function NativeQrcode() {
}

NativeQrcode.open = function(params,callbackContext) {
    callbackContext = callbackContext || {};
    cordova.exec(callbackContext.success || null, callbackContext.error || null, "NativeQrcode", "open", [params]);

};

/**
 * Load Plugin
 */

// if(!window.plugins) {
//     window.plugins = {};
// }
// if (!window.plugins.openNative) {
//     window.plugins.openNative = new OpenNative();
// }

NativeQrcode.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.NativeQrcode = new NativeQrcode();
  return window.plugins.NativeQrcode;
};

cordova.addConstructor(NativeQrcode.open);

module.exports = NativeQrcode;